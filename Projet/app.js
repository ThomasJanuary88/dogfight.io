var express = require('express');
var app = express();
var serv = require('http').Server(app);

app.use('/', express.static(__dirname + '/client'));

var io = require('socket.io')(serv, {});

var players = {};
let maxSpawnX = 2000;
let maxSpawnY = 2000;

io.sockets.on('connection', (socket) => {
	console.log('connect : ' + socket.id);

	var player = {};
	player.name =  socket.handshake.query.name;
	player.img = socket.handshake.query.img;
	player.score = 0;
	let pos = spawnPlayer();
	player.x = pos[0];
	player.y = pos[1];
	player.nameColor = generateRandomHexaColor();
	players[socket.id] = player;

	socket.emit('getOldPlayers', {
		players : players,
		id : socket.id
	});

	socket.emit('playerInformation', {
		id : socket.id,
		name : player.name,
		img : player.img,
		x : player.x,
		y : player.y,
		score : player.score,
		nameColor : player.nameColor
	});

	socket.broadcast.emit('newPlayer' , {
		id : socket.id,
		name : player.name,
		img : player.img,
		x : player.x,
		y : player.y,
		score : player.score,
		nameColor : player.nameColor
	});

	socket.on('updatePlayerInformation', (data) => {
		if (typeof data !== "undefined") {
			if (typeof players[socket.id] !== "undefined") {
				players[socket.id].x = data.x;
				players[socket.id].y = data.y;
				players[socket.id].angle = data.angle;
				players[socket.id].score = data.score;
				players[socket.id].shieldHp = data.shieldHp;
			}
		}
	});

	socket.on('newProjectile', (data) => {
		socket.broadcast.emit('addOtherProjectile', {
			x: data.x,
			y: data.y,
			angle: data.angle,
			tag: 'otherPlayer',
			type: data.type
		});
	});

	socket.on('shieldState', (data) => {
		let activate = data.activate;
		socket.broadcast.emit('addOtherShield', {
			id : socket.id,
			shieldState : activate
		})
	});

	socket.on('disconnect' , () => {
        socket.broadcast.emit('disconnectedPlayer', {
        	id : socket.id
        });
        delete players[socket.id];
	});

	socket.on('gameOver', () => {
		socket.broadcast.emit('disconnectedPlayer', {
			id : socket.id
		})
		console.log('disctionnected : ' + socket.id);
		socket.disconnect();
        delete players[socket.id];
	});

});

setInterval(() => {
	io.sockets.emit('updatePlayersInformations', {
		players : players
	});
}, 15);

function spawnPlayer() {
	return [Math.floor((Math.random() * maxSpawnX) + 100), Math.floor((Math.random() * maxSpawnY) +100)];
}

serv.listen(2000);

function generateRandomHexaColor() {
	return '#'+(Math.random()*0xFFFFFF<<0).toString(16);
}