/*
	Le controller est l'objet permettant de faire le lien
	entre l'objet view et l'objet model.

	Il sert aussi de point d'entré pour le serveur, mettant à
	jour l'objet model avec les nouvelles data.
*/

class Controller {
	constructor () {
		this.model = new Model(this);
		this.view = new View(this);
	}
	tick() {
		this.model.tick();
		this.view.tick();
		window.requestAnimationFrame(() => {this.tick(); });
	}

	gameOver() {
		cancelAnimationFrame(this.requestTick);
		this.io.emit('gameOver');
		top.location.reload(1);
	}

	// Point d'entré au serveur, lancement du tick du controller
	joinGame(img, name) {
		this.io = io({
			query: {
				img : img,
				name : name
			}
		});
		this.ioCommunication();
		this.startGame = setTimeout(() => {
			this.requestTick = window.requestAnimationFrame(() => {this.tick();});
			this.model.startEmitInformation();
		},500);
	}

	ioCommunication() {
		this.io.on('playerInformation', data => {
			this.model.createPlayer(data);
			this.view.setCameraPosition(data.x, data.y);
			this.model.setBounds();
		});
		this.io.on('newPlayer', data => {
			this.model.createOtherPlayer(data.id, data);
		});
		this.io.on('getOldPlayers', data => {
			let players = data['players'];
			for (let key in players) {
				if (key != data.id) {
					this.model.createOtherPlayer(key, players[key]);
				}
			}
		});
		this.io.on('updatePlayersInformations', (data) => {
			let players = data['players'];
			for (let key in players) {
				if (key != this.model.player.id) {
					this.model.updateOtherPlayer(key, players[key]);
				}
			}
		});
		this.io.on('disconnectedPlayer', (data) => {
			this.model.deleteOtherPlayer(data);
		});
		this.io.on('addOtherProjectile', (data) => {
			this.model.createOtherProjectile(data);
		});
		this.io.on('addOtherShield', (data) => {
			this.model.createOtherShield(data);
		});
	}

	handleInput(button) {
		this.model.handleInput(button);
	}

	setMousePosition(x, y) {
		this.model.setMousePosition(x, y);
	}

	emitShield(boolean) {
		this.io.emit('shieldState', {
			activate: boolean
		});
	}

	emitProjectile(projectile, type){
		this.io.emit('newProjectile', {
			x: projectile.x,
			y: projectile.y,
			angle: projectile.angle,
			type: type
    	});
	}

	emitUpdatePlayerInformation(player) {
    	this.io.emit('updatePlayerInformation', {
			x : player.spaceship.x,
			y : player.spaceship.y,
			angle : player.spaceship.angle,
			score : player.score,
			shieldHp : player.spaceship.shield.hp
    	});
	}
}