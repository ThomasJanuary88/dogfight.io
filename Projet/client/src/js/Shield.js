class Shield{
	constructor(spaceship) {
		this.spaceship = spaceship;
		this.width = 144;
		this.height = 137;
		this.x = this.spaceship.x - this.width/2;
		this.y = this.spaceship.y - this.height/2;
		this.img = new Image(this.x, this.y);
		this.img.src = "../assets/graphics/shield/shield.png";
		this.activate = false;
		this.maxHp = 100;
		this.hp = this.maxHp;
		this.defaultHealthQuantite = 10;
		this.defaultLostHealthQuantite = 5;
		this.healthQuantite = this.defaultHealthQuantite;
		this.lostHealthQuantite = this.defaultLostHealthQuantite;
		this.energy = 2;
		this.repairNeed = false;
		this.nextHealth = 0;
		this.healthRate = 10;
	}
	useShield () {
		if (!this.repairNeed) {
			if (this.activate) {
				this.activate = false;
			}
			else {
				this.activate = true;
			}
		}
		else {
			this.activate = false;
		}
	}
	updatePower() {
		if (this.energy == 0) {
			this.lostHealthQuantite = this.defaultLostHealthQuantite * 1.5;
		}
		else if (this.energy == 1) {
			this.lostHealthQuantite = this.defaultLostHealthQuantite * 1.25;
		}
		else if (this.energy == 2) {
			this.lostHealthQuantite = this.defaultLostHealthQuantite;
		}
		else if (this.energy == 3) {
			this.lostHealthQuantite = this.defaultLostHealthQuantite * 0.75;
		}
		else if (this.energy == 4) {
			this.lostHealthQuantite = this.defaultLostHealthQuantite * 0.5;
		}
	}
	tick() {
		this.x = this.spaceship.x - this.width/2;
		this.y = this.spaceship.y - this.height/2;
		if (this.repairNeed) {
			if (this.hp >= this.maxHp) {
				this.repairNeed = false;
			}
		}
		if (this.nextHealth > 0) {
			this.nextHealth -= 1;
		}
		if (this.nextHealth == 0) {
			if (this.activate && !this.repairNeed) {
				this.hp -= this.lostHealthQuantite;
			}
			else if (this.activate && this.repairNeed && this.hp < this.maxHp) {
				this.hp += this.lostHealthQuantite;
			}
			else if (!this.activate && this.hp < this.maxHp) {
				this.hp += this.healthQuantite;
				if (this.hp > this.maxHp) {
					this.hp = this.maxHp;
				}
			}
        	this.nextHealth = this.healthRate;
		}
		if (this.hp <= 0) {
			this.repairNeed = true;
			this.activate = false;
			this.hp = 0;
		}
	}
}