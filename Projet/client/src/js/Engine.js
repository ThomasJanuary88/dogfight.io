class Engine{
	constructor(spaceship) {
		this.spaceship = spaceship;
		this.energy = 2;
		this.defaultMaxVelocity = 6;
		this.defaultMinVelocity = -6;
		this.defaultSpeed = 0.22;
		this.spaceship.maxVelocity = this.defaultMaxVelocity;
		this.spaceship.minVelocity = this.defaultMinVelocity;
		this.speed = this.defaultSpeed;
	}

	updatePower() {
		if (this.energy == 0) {
			this.speed = this.defaultSpeed * 0.5;
			this.spaceship.minVelocity = this.defaultMinVelocity * 0.5;
			this.spaceship.maxVelocity = this.defaultMaxVelocity * 0.5;
			
		}
		else if (this.energy == 1) {
			this.speed = this.defaultSpeed * 0.75;
			this.spaceship.minVelocity = this.defaultMinVelocity * 0.75;
			this.spaceship.maxVelocity = this.defaultMaxVelocity * 0.75;
		}
		else if (this.energy == 2) {
			this.speed = this.defaultSpeed;
			this.spaceship.minVelocity = this.defaultMinVelocity;
			this.spaceship.maxVelocity = this.defaultMaxVelocity;
		}
		else if (this.energy == 3) {
			this.speed = this.defaultSpeed * 1.25;
			this.spaceship.minVelocity = this.defaultMinVelocity * 1.25;
			this.spaceship.maxVelocity = this.defaultMaxVelocity * 1.25;
		}
		else if (this.energy == 4) {
			this.speed = this.defaultSpeed * 1.5;
			this.spaceship.minVelocity = this.defaultMinVelocity * 1.5;
			this.spaceship.maxVelocity = this.defaultMaxVelocity * 1.5;
		}
	}
}