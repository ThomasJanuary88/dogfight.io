class View {
	constructor(controller) {
		this.controller = controller;
		this.inputs = {
			"KeyW" : false,
			"KeyD" : false,
			"KeyS" : false,
			"KeyA" : false,
			"KeyQ" : false,
			"Digit1" : false,
			"Digit2" : false,
			"Digitq" : false,
			"Digit3" : false,
			"KeyR" : false,
			"leftClick" : false,
			'Space' : false
		};
		this.screenWidth = screen.width;
		this.screenHeight = screen.height

		// Layer du menu
		this.menuScreen = document.getElementById("menuScreen");
		this.spaceshipSelectorLeftButton = document.getElementById("spaceshipSelectorLeftButton");
		this.spaceshipSelectorRightButton = document.getElementById("spaceshipSelectorRightButton");
		this.spaceshipSelectorView = document.getElementById("spaceshipSelectorView");
		this.playerName = document.getElementById("nameInput");
		this.launchGameButton = document.getElementById('launchGameButton');
		// Layer du jeu
		this.gameScreen = document.getElementById("gameScreen");
		this.canvas = document.getElementById("canvas");
		this.context = this.canvas.getContext('2d');
		this.spaceshipState = document.getElementById('spaceshipState');
		this.tableScore = document.getElementById('tableScore');

		this.switchScreen("screenMenu");
		this.addMenuEventListener();
		this.addGameEventListener();
		this.addUserInterface();
	}

	tick() {
		this.draw();
		this.handleInput();
	}

	// Changer d'ecrans
	switchScreen(screen) {
		switch (screen) {
			case "screenGame":
				this.menuScreen.style.visibility = 'hidden';
				this.gameScreen.style.visibility = "visible";


			break;

			case "screenMenu":
				this.gameScreen.style.visibility = 'hidden';
				this.menuScreen.style.visibility = 'visible';


				// Affiche le premier vaisseau
				this.spaceshipSelectorView.style.backgroundImage = "url('assets/graphics/spaceship/spaceship0.png')";

				this.currentSpaceship = 0;
			break;
		}
	}

	setCameraPosition(x, y) {
		x = x - this.controller.model.player.spaceship.width /2;
		y = y - this.controller.model.player.spaceship.height /2;
		x-= this.screenWidth/2 + this.controller.model.player.spaceship.width/2;
		y-= this.screenHeight/2 +this.controller.model.player.spaceship.height/2;
		this.xCamera = x;
		this.yCamera = y;
	}

	// Affiche le prochain vaisseau ou le precedent
	chooseSpaceship(selectOption) {
		switch (selectOption) {
			case "next" :
				if (this.currentSpaceship == 6) {
					this.currentSpaceship = 0;
				}
				else {
					this.currentSpaceship += 1;
				}
			break;

			case "previous" :
				if (this.currentSpaceship == 0) {
					this.currentSpaceship = 6;
				}
				else {
					this.currentSpaceship -= 1;
				}
			break;
		}
		this.spaceshipSelectorView.style.backgroundImage = "url('assets/graphics/spaceship/spaceship"+this.currentSpaceship+".png')";		
	}

	addMenuEventListener() {
		this.spaceshipSelectorLeftButton.addEventListener("mousedown", () => {
			this.chooseSpaceship("next");
		}, false);

		this.spaceshipSelectorRightButton.addEventListener("mousedown", () => {
			this.chooseSpaceship("previous");
		}, false);

		this.launchGameButton.addEventListener("click", ()=>{this.joinGame();}, true);
	}

	joinGame() {
		let name = this.playerName.value;
		let spaceshipImage = this.spaceshipSelectorView.style.backgroundImage;
		let imagePath = spaceshipImage.substr(5, spaceshipImage.length-7);
		this.controller.joinGame(imagePath, name);
		this.launchGameButton.blur();
		this.switchScreen("screenGame");
	}

	addGameEventListener() {
		document.addEventListener("keypress", e => {
			if (e.code in this.inputs) {
		    	this.inputs[e.code] = true;
			}
		});

		document.addEventListener("keyup", e => {
			if (e.code in this.inputs) {
		    	this.inputs[e.code] = false;
		    	
		    	// Plus facile de faire comme ca pour l'instant ...
		    	this.controller.model.keyUp(e.code);
			}
		});

		document.addEventListener('mousemove', (event)=> {
			this.controller.setMousePosition(event.pageX, event.pageY);
		}, false);


		document.addEventListener('mousedown', (e) => {
			if (e.which == 1) {
	    		this.inputs['leftClick'] = true;
			}
	    });
	    document.addEventListener('mouseup', (e) => {
    		this.inputs['leftClick'] = false;
	    });
	}

	addUserInterface() {
		this.UIShieldHp = new ProgressBar.Circle('#shieldHp', {
			strokeWidth: 6,
			easing: 'easeInOut',
			duration: 1400,
			color: '#3385ff',
			trailColor: '#eee',
			trailWidth: 2,
			svgStyle: null
		});

		this.UISpaceshipHp = new ProgressBar.Circle('#spaceshipHp', {
			strokeWidth: 6,
			easing: 'easeInOut',
			duration: 1400,
			color: '#FFEA82',
			trailColor: '#eee',
			trailWidth: 2,
			svgStyle: {width: '100%', height: '100%'},
			from: {color: '#ff6666'},
			to: {color: '#79ff4d'},
			step: (state, bar) => {
			bar.path.setAttribute('stroke', state.color);
			}
		});

		this.UISpaceshipWeapon = new ProgressBar.Line('#spaceshipWeapon', {
			strokeWidth: 4,
			easing: 'easeInOut',
			duration: 1400,
			color: '#FFEA82',
			trailColor: '#eee',
			trailWidth: 1,
			svgStyle: {width: '100%', height: '100%'}
		});

		this.UISpaceshipShield = new ProgressBar.Line('#spaceshipShield', {
			strokeWidth: 4,
			easing: 'easeInOut',
			duration: 1400,
			color: '#FFEA82',
			trailColor: '#eee',
			trailWidth: 1,
			svgStyle: {width: '100%', height: '100%'}
		});

		this.UISpaceshipEngine = new ProgressBar.Line('#spaceshipEngine', {
			strokeWidth: 4,
			easing: 'easeInOut',
			duration: 1400,
			color: '#FFEA82',
			trailColor: '#eee',
			trailWidth: 1,
			svgStyle: {width: '100%', height: '100%'}
		});

		this.UICanonLaserOverHeat = new ProgressBar.SemiCircle('#canonLaserOverHeat', {
  			strokeWidth: 6,
		 	color: '#FFEA82',
			trailColor: '#eee',
			trailWidth: 1,
			easing: 'easeInOut',
			duration: 1400,
			svgStyle: null,
			text: {
		    	value: '',
		    	alignToBottom: false
		    },
		  	from: {color: '#FFEA82'},
		  	to: {color: '#ED6A5A'},
		  	// Set default step function for all animate calls
		  	step: (state, bar) => {
		    	bar.path.setAttribute('stroke', state.color);
		  	}
		});

		this.UIMachineGunAmmo = new ProgressBar.SemiCircle('#machineGunAmmo', {
  			strokeWidth: 6,
		 	color: '#FFEA82',
			trailColor: '#eee',
			trailWidth: 1,
			easing: 'easeInOut',
			duration: 1400,
			svgStyle: null,
			text: {
		    	value: '',
		    	alignToBottom: false
		    },
		  	from: {color: '#FFEA82'},
		  	to: {color: '#ED6A5A'}
		});

		this.UISpaceshipWeapon.animate(0.5);
		this.UISpaceshipShield.animate(0.5);
		this.UISpaceshipEngine.animate(0.5);
		this.UIShieldHp.animate(1.0);
		this.UISpaceshipHp.animate(1.0);
	}

	handleInput() {	
		for (let key in this.inputs) {
			if (this.inputs[key] === true) {
				this.controller.handleInput(key);
			}
		}
	}

	draw() {
		this.context.globalAlpha = 1;
		// Récupération des données à dessiner depuis le model
		let model = this.controller.model;
		let player = model.player;
		let playerSpaceship = player.spaceship;
		let playerSpaceshipShield = playerSpaceship.shield;
		let entities = model.entities;

		// Reduire la zone de raffraichissement du canvas
		let x1, x2, y1, y2;
		x1 = window.pageXOffset;
		y1 = window.pageYOffset;
		x2 = x1 + this.screenWidth;
		y2 = y1 + this.screenHeight;

		if (x1 > 0) {
			x1 -= 75;
		}
		if (y1 > 0) {
			y1 -=75;
		}

		// Mettre a jours le tableau des scores
		this.updateTableScore(entities);

		//Mise a jours de la position de la souris quand on ne bouge pas la souris
		this.controller.model.xMouse += playerSpaceship.xVelocity;
		this.controller.model.yMouse += playerSpaceship.yVelocity;

		// Effacer le canvas
		this.context.clearRect(x1, y1, x2, y2);
		this.context.beginPath();


		// Bouger la camera
		this.xCamera += playerSpaceship.xVelocity;
		this.yCamera += playerSpaceship.yVelocity;
		window.scrollTo(this.xCamera, this.yCamera);

		// Dessiner le vaisseau du joueur
		this.rotateDrawImage(playerSpaceship);
		// Dessiner le nom du joueur
		this.context.font="20px Book";
		this.context.fillStyle = player.nameColor;
		this.context.textAlign = "center";
		this.context.fillText(player.name, playerSpaceship.x, playerSpaceship.y - playerSpaceship.height*0.75);

		for (let key in entities['otherPlayer']) {
			this.context.fillStyle = entities['otherPlayer'][key].nameColor;
			this.context.fillText(entities['otherPlayer'][key].name, entities['otherPlayer'][key].x, entities['otherPlayer'][key].y - entities['otherPlayer'][key].height*0.75);
		}

		// Mise à jours de la mini-map
		this.updateMiniMap(entities, playerSpaceship);

		// Mise à jours de l'interface utilisateur
		this.UIShieldHp.set(playerSpaceshipShield.hp/playerSpaceshipShield.maxHp);
		this.UISpaceshipHp.set(playerSpaceship.hp/playerSpaceship.baseHp);
		this.UISpaceshipWeapon.set(playerSpaceship.weaponsEnergy/playerSpaceship.generator.maxEnergyPerParts);
		this.UISpaceshipShield.set(playerSpaceshipShield.energy/playerSpaceship.generator.maxEnergyPerParts);
		this.UISpaceshipEngine.set(playerSpaceship.engine.energy/playerSpaceship.generator.maxEnergyPerParts);
		this.UICanonLaserOverHeat.set(playerSpaceship.weapons[0].overheat/playerSpaceship.weapons[0].maxOverheat);
		this.UIMachineGunAmmo.setText(this.controller.model.player.spaceship.weapons[1].magazine + "/" + this.controller.model.player.spaceship.weapons[1].defaultMagazine);
		if (playerSpaceship.weapons[1].isReloading) {
			this.UIMachineGunAmmo.set(playerSpaceship.weapons[1].nextReload/playerSpaceship.weapons[1].reloadRate);
		}
		else {
			this.UIMachineGunAmmo.set(0);
		}

		if (playerSpaceshipShield.activate) {
			this.drawImage(playerSpaceshipShield);
		}

		for (let key in entities) {
			switch (key) {
				case "projectile":
					for (let i = 0; i < entities["projectile"].length; i++) {
						let projectile = entities["projectile"][i];
						this.rotateDrawImage(projectile);
					}
				break;

				case "showDamageDealt" :
					for (let i = 0; i < entities["showDamageDealt"].length; i++) {
						let showDamageDealt = entities["showDamageDealt"][i];
						this.context.fillStyle = showDamageDealt.color;
						this.context.globalAlpha  = showDamageDealt.alpha;
						this.context.font="30px Book";
						this.context.fillText(showDamageDealt.text, showDamageDealt.x, showDamageDealt.y);
					}
				break;

				case "otherPlayer":
					for (let otherPlayer in entities["otherPlayer"]) {
						this.rotateDrawImage(entities["otherPlayer"][otherPlayer]);
						if (entities["otherPlayer"][otherPlayer].shieldActivate) {
							this.drawImage(entities["otherPlayer"][otherPlayer].shield);
						}
					}
				break;
			}
		}
	}

	updateTableScore(entities) {
		//clear
		let mainNode = document.getElementById("scoreTable");
		while (mainNode.firstChild) {
		    mainNode.removeChild(mainNode.firstChild);
		}

		let otherPlayersScore = [];
		otherPlayersScore.push([this.controller.model.player.name, this.controller.model.player.score, this.controller.model.player.nameColor]);
		for (let key in entities['otherPlayer']) {
			otherPlayersScore.push([entities['otherPlayer'][key].name, entities['otherPlayer'][key].score, entities['otherPlayer'][key].nameColor]);
		}

		otherPlayersScore = otherPlayersScore.sort(function (a, b) {
			return a[1] < b[1];
		});

		let template = document.querySelector("#template");

		let rank = 0;
		for (let i in otherPlayersScore) {
			rank += 1;
			if (rank == 10) {
				break;
			}
			let node1 = document.createElement("p");

			node1.className = 'playerRank';
			node1.innerHTML = "#" + rank +".\t" + otherPlayersScore[i][0];

			for (let j = 0; j < (11 - otherPlayersScore[i][0].length); j++) {
				node1.innerHTML += "&nbsp;";
			}
			node1.innerHTML += otherPlayersScore[i][1];
			node1.style.color = otherPlayersScore[i][2];
			document.getElementById("scoreTable").appendChild(node1);

			let br = document.createElement("br");
			document.getElementById("scoreTable").appendChild(br);
		}

	}

	updateMiniMap(entities, playerSpaceship) {
		//clear
		let mainNode = document.getElementById("miniMap");
		while (mainNode.firstChild) {
		    mainNode.removeChild(mainNode.firstChild);
		}


		let node = document.createElement("div");
		node.className = 'playerMiniMap';
		node.style.top = playerSpaceship.y / 12.5 + "px";
		node.style.left = playerSpaceship.x / 12.5 + "px";

		document.getElementById("miniMap").appendChild(node);

		for (let key in entities['otherPlayer']) {
			let node = document.createElement("div");
			node.className = 'otherPlayerMiniMap';
			node.style.top = entities['otherPlayer'][key].y / 12.5 + "px";
			node.style.left = entities['otherPlayer'][key].x / 12.5 + "px";

			document.getElementById("miniMap").appendChild(node);
		}
	}

	rotateDrawImage(entity) {
	 	this.context.setTransform(1, 0, 0, 1, entity.x, entity.y);
	    this.context.rotate(entity.angle);
	    this.context.drawImage(entity.img, -entity.width/2, -entity.height/2);
	    this.context.setTransform(1, 0, 0, 1, 0, 0); 
	}


	drawImage(entity) {
		let width = entity.hp + 50;
		let height = entity.hp + 50;
		let x = entity.x + 75 - width / 2;
		let y = entity.y + 75 - height / 2;
	    this.context.drawImage(entity.img, x, y, width, height);
    }

    playSound(path, volume=0.1) {
		let a = new Audio(path);
		a.volume = volume;
		a.play();
	}

}