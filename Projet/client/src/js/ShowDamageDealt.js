class ShowDamageDealt {
	constructor(x, y, text, statut) {
		this.x = x;
		this.y = y - 90;
		this.text = ("- " + text);
		this.alive = true;
		if (statut == "shieldActivate") {
			this.color = "#ffffff";
		}
		else {
			this.color = "#ff3300";
		}
		this.alpha = 1;
		this.dieAfter();
	}
	dieAfter() {
		setTimeout(() => {
			this.alive = false;
		}, 700);
	}
	tick() {
		this.alpha -= 0.02;
		this.y -= 3;
	}
}