class Projectile extends Entity {
	constructor (x, y, damage, imgPath, angle, width, height, tag) {
		super(x, y, 0, 0, -5, 5, angle, imgPath, width, height);
		this.tag = tag;
		this.damage = damage;
		if (tag == "mine") {
			this.updatePosition();
		}
		this.updateVelocity();
	}
	tick() {
		super.tick();
	}
	updateVelocity() {
		let speed = 30;
		this.xVelocity += speed * Math.cos(this.angle);
		this.yVelocity += speed * Math.sin(this.angle);
	}
	updatePosition() {
		this.x += 40 * Math.cos(this.angle);
		this.y += 40 * Math.sin(this.angle);
	}
}