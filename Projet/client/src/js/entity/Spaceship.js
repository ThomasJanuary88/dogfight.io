class Spaceship extends Entity {
	constructor(x, y, imgPath) {
		super(x, y, 0, 0, -10, 10, 0, imgPath, 75, 99);
		this.x = this.x - this.width;
		this.y = this.y - this.height;
		this.baseHp = 100;
		this.maxHp = this.baseHp;
		this.hp = this.maxHp;
		this.healthQuantite = 0.5;
		this.engine = new Engine(this);
		this.shield = new Shield(this);

		this.weaponsEnergy = 2;
		this.weapons = [new LaserCannon(this), new MachineGun(this)];
		this.weapons[0].unsheatheWeapon();
		this.generator = new Generator(this);

		this.goToRight = false;
		this.goToLeft = false;
		this.goToUp = false;
		this.goToDown = false;
		if (imgPath == "assets/graphics/spaceship/spaceship0.png") {
			this.imgHit = "assets/graphics/spaceship/spaceship0Hit.png";
		}
		else if (imgPath == "assets/graphics/spaceship/spaceship1.png") {
			this.imgHit = "assets/graphics/spaceship/spaceship1Hit.png";
		}
		else if (imgPath == "assets/graphics/spaceship/spaceship2.png") {
			this.imgHit = "assets/graphics/spaceship/spaceship2Hit.png";
		}
		else if (imgPath == "assets/graphics/spaceship/spaceship3.png") {
			this.imgHit = "assets/graphics/spaceship/spaceship3Hit.png";
		}
		else if (imgPath == "assets/graphics/spaceship/spaceship4.png") {
			this.imgHit = "assets/graphics/spaceship/spaceship4Hit.png";
		}
		else if (imgPath == "assets/graphics/spaceship/spaceship5.png") {
			this.imgHit = "assets/graphics/spaceship/spaceship5Hit.png";
		}
		else if (imgPath == "assets/graphics/spaceship/spaceship6.png") {
			this.imgHit = "assets/graphics/spaceship/spaceship6Hit.png";
		}

		this.nextHealth = 0;
		this.healthRate = 10;
	}
	moveUp() {
		if (this.goToDown) {
			this.yVelocity += this.engine.speed;
		}
		if (this.yVelocity > this.minVelocity) {
			this.yVelocity -= this.engine.speed;
		}
		this.goToDown = false;
		this.goToUp = true;
	}
	moveDown() {
		if (this.goToUp) {
			this.yVelocity -= this.engine.speed;
		}
		if (this.yVelocity < this.maxVelocity) {
			this.yVelocity += this.engine.speed;
		}
		this.goToDown = true;
		this.goToUp = false;
	}
	moveRight() {
		if (this.goToLeft) {
			this.xVelocity -= this.engine.speed;
		}
		if (this.xVelocity < this.maxVelocity) {
			this.xVelocity += this.engine.speed;
		}
		this.goToRight = true;
		this.goToLeft = false;
	}
	moveLeft() {
		if (this.goToRight) {
			this.xVelocity += this.engine.speed;
		}
		if (this.xVelocity > this.minVelocity) {
			this.xVelocity -= this.engine.speed;
		}
		this.goToRight = false;
		this.goToLeft = true;
	}
	useShield() {
		this.shield.useShield();
	}
	boostWeapons() {
		this.generator.boostWeapons();
	}
	boostEngine() {
		this.generator.boostEngine();
	}
	boostShield() {
		this.generator.boostShield();
	}
	switchWeapon() {
		if (this.weapons[0].unsheathe) {
			this.weapons[0].sheatheWeapon();
			this.weapons[1].unsheatheWeapon();
		}
		else {
			this.weapons[0].unsheatheWeapon();
			this.weapons[1].sheatheWeapon();
		}
	}
	fire() {
		if (this.weapons[0].unsheathe) {
			this.weapons[0].fire();
		}
		if (this.weapons[1].unsheathe) {
			this.weapons[1].fire();
		}
	}
	tick() {
		super.tick();
		if (this.hp <= 75) {
			this.maxHp = 75;
		}
		if (this.hp <= 50) {
			this.maxHp = 50;
		}
		if (this.hp <= 25) {
			this.maxHp = 25;
		}
		if (this.hp < this.maxHp) {
			if (this.nextHealth > 0) {
				this.nextHealth -= 1;
			}
			if (this.nextHealth == 0) {
				this.hp += this.healthQuantite;
				this.nextHealth = this.healthRate;
			}
		}

		for (let i =0; i < this.weapons.length; i++) {
			this.weapons[i].tick();
		}
		this.shield.tick();
	}
	hit(damage) {
		if (this.shield.activate) {
			this.shield.hp -= damage;
		}
		else {
			this.hp -= damage;
			this.img.src = this.imgHit;
			setTimeout(() => {
				this.img.src = this.imgInit;
			}, 300);
		}
	}
}