class OtherPlayer extends Entity{
	constructor(x, y, name, imgPath, score, nameColor) {
		super(x, y, 0, 0, 0, -10, 10, imgPath, 75,99);
		this.name = name;
		this.shield = new OtherPlayerShield(this);
		this.shieldActivate = false;
		this.score = score;
		this.nameColor = nameColor;

		if (imgPath == "assets/graphics/spaceship/spaceship0.png") {
			this.imgHit = "assets/graphics/spaceship/spaceship0Hit.png";
		}
		else if (imgPath == "assets/graphics/spaceship/spaceship1.png") {
			this.imgHit = "assets/graphics/spaceship/spaceship1Hit.png";
		}
		else if (imgPath == "assets/graphics/spaceship/spaceship2.png") {
			this.imgHit = "assets/graphics/spaceship/spaceship2Hit.png";
		}
		else if (imgPath == "assets/graphics/spaceship/spaceship3.png") {
			this.imgHit = "assets/graphics/spaceship/spaceship3Hit.png";
		}
		else if (imgPath == "assets/graphics/spaceship/spaceship4.png") {
			this.imgHit = "assets/graphics/spaceship/spaceship4Hit.png";
		}
		else if (imgPath == "assets/graphics/spaceship/spaceship5.png") {
			this.imgHit = "assets/graphics/spaceship/spaceship5Hit.png";
		}
		else if (imgPath == "assets/graphics/spaceship/spaceship6.png") {
			this.imgHit = "assets/graphics/spaceship/spaceship6Hit.png";
		}
	}
	tick() {
		super.tick();
		if (this.shieldActivate) {
			this.shield.tick();
		}
	}
	useShield(state) {
		this.shieldActivate = state;
	}
	hit () {
		this.img.src = this.imgHit;
		setTimeout(() => {
			this.img.src = this.imgInit;
		}, 300);
	}
}

class OtherPlayerShield {
	constructor(spaceship) {
		this.spaceship = spaceship;
		this.width = 144;
		this.height = 137;
		this.x = this.spaceship.x;
		this.y = this.spaceship.y;
		this.img = new Image(this.x, this.y);
		this.img.src = "../assets/graphics/shield/shield.png";
		this.hp = 100;
	}
	tick() {
		this.x = this.spaceship.x - this.width/2;
		this.y = this.spaceship.y - this.height/2;
	}
}