class Bullet extends Projectile {
	constructor (x, y, angle, tag) {
		super(x, y, 4, "../../assets/graphics/projectile/bullet.png", angle, 17, 17, tag);
		this.dieAfter();
	}
	tick () {
		super.tick();
	}
	dieAfter() {
		setTimeout(() => {
			this.alive = false;
		}, 450);
	}
}