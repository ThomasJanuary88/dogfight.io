class Entity {
	constructor(x, y, xVelocity, yVelocity, minVelocity, maxVelocity, angle, imgPath, width, height) {
		this.x = x;
		this.y = y;
		this.xVelocity = xVelocity;
		this.yVelocity = yVelocity;
		this.minVelocity = minVelocity;
		this.maxVelocity = maxVelocity;
		this.img = new Image(this.x, this.y);
		this.imgInit = imgPath;
		this.img.src = this.imgInit;
		this.width = width;
		this.height = height;
		this.angle = angle;
		this.alive = true;
	}
	tick() {
		if (this.touchDownBoundary) {
			this.yVelocity = 0;
		}
		this.y += this.yVelocity;
		this.x += this.xVelocity;
	}
}