class Laser extends Projectile {
	constructor (x, y, angle, tag) {
		super(x, y, 4, "../../assets/graphics/projectile/laserRed01.png", angle, 54, 9, tag);
		this.dieAfter();
	}
	tick () {
		super.tick();
	}
	dieAfter() {
		setTimeout(() => {
			this.alive = false;
		}, 450);
	}
}