
class MachineGun extends Weapon {
	constructor(spaceship) {
		super();
		this.spaceship = spaceship;
		this.defaultMagazine = 15;
		this.magazine = this.defaultMagazine;
		this.reloadNeed = false;
		this.isReloading = false;
		this.nextFire = 0;
		this.fireRate = 10;
		this.nextReload = 120;
		this.reloadRate = 120;
		this.fired = false;
	}
	fire () {
		if (this.nextFire > 0) {
			this.nextFire -= 1;
		}
		if (this.nextFire == 0) {
			if (!this.reloadNeed && !this.isReloading) {
				if (this.magazine <= 0) {
					this.reloadNeed = true;
				}
				else {
					this.magazine -= 1;

					this.fired = true;
					this.nextFire = this.fireRate;
				}
			}
			else {
				if (!this.isReloading) {
					this.isReloading = true;
				}
			}
		}
	}

	createProjectile() {
		this.fired = false;
		return (new Bullet(this.spaceship.x, this.spaceship.y, this.spaceship.angle, "mine"));
	}

	updatePower() {
		super.updatePower();
	}
	reload() {
		if (this.nextReload > 0) {
			this.nextReload -= 1;
		}
		if (this.nextReload == 0) {
			this.isReloading = false;
			this.magazine = this.defaultMagazine;
			this.nextReload = this.reloadRate;
		}
	}
	tick() {
		super.tick();
		if (this.isReloading) {
			this.reloadNeed = false;
			this.reload();
		}
	}
	unsheatheWeapon() {
		super.unsheatheWeapon();
	}
	sheatheWeapon() {
		super.sheatheWeapon();
	}
	updatePower() {
		if (this.spaceship.weaponsEnergy == 0) {
			this.defaultMagazine = 5;
			this.reloadRate = 160;
			this.fireRate = 17;
			if (!this.reloadNeed) {
				this.nextReload = 160;
			}
		}
		else if (this.spaceship.weaponsEnergy == 1) {
			this.defaultMagazine = 10;
			this.reloadRate = 140;
			this.fireRate = 15;
			if (!this.reloadNeed) {
				this.nextReload = 140;
			}
		}
		else if (this.spaceship.weaponsEnergy == 2) {
			this.defaultMagazine = 15;
			this.reloadRate = 120;
			this.fireRate = 13;
			if (!this.reloadNeed) {
				this.nextReload = 120;
			}
		}
		else if (this.spaceship.weaponsEnergy == 3) {
			this.defaultMagazine = 20;
			this.reloadRate = 100;
			this.fireRate = 11;
			if (!this.reloadNeed) {
				this.nextReload = 100;
			}
		}
		else if (this.spaceship.weaponsEnergy == 4) {
			this.defaultMagazine = 25;
			this.reloadRate = 80;
			this.fireRate = 9;
			if (!this.reloadNeed) {
				this.nextReload = 80;
			}
		}

		if (this.magazine > this.defaultMagazine) {
			this.magazine = this.defaultMagazine;
		}
	}
}