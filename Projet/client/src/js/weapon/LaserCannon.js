class LaserCannon extends Weapon{
	constructor(spaceship) {
		super();
		this.spaceship = spaceship;
		this.overheat = 0;
		this.maxOverheat = 20;
		this.coolingNeed = false;
		this.coolingRate = 10;
		this.nextCooling = 0;
		this.coolingQuantite = 1;
		this.overheatQuantite = 2;
		this.nextFire = 0;
		this.fireRate = 10;
		this.fired = false;
	}
	tick() {
		super.tick();
		this.autoCooling();
	}
	fire() {
		if (this.nextFire > 0) {
			this.nextFire -= 1;
		}
		if (this.nextFire <= 0) {
			if (!this.coolingNeed) {
				this.overheat += this.overheatQuantite;
				if (this.overheat >= this.maxOverheat) {
					this.coolingNeed = true;
				}
				else {
					this.fired = true;
					this.nextFire = this.fireRate;
				}
			}
			else {
				if (this.overheat <= 0) {
					this.coolingNeed = false;
				}
			}
		}
		if (this.overheat > this.maxOverheat) {
			this.overheat = this.maxOverheat;
		}
	}
	createProjectile() {
		this.fired = false;
		return new Laser(this.spaceship.x, this.spaceship.y, this.spaceship.angle, "mine");
	}

	updatePower() {
		if (this.spaceship.weaponsEnergy == 0) {
			this.coolingQuantite = 1.2;
			this.overheatQuantite = 5;
			this.fireRate = 17;
		}
		else if (this.spaceship.weaponsEnergy == 1) {
			this.coolingQuantite = 1.4;
			this.overheatQuantite = 4.5;
			this.fireRate = 15;
		}
		else if (this.spaceship.weaponsEnergy == 2) {
			this.coolingQuantite = 1.6;
			this.overheatQuantite = 4;
			this.fireRate = 13;
		}
		else if (this.spaceship.weaponsEnergy == 3) {
			this.coolingQuantite = 1.8;
			this.overheatQuantite = 3.5;
			this.fireRate = 11;
		}
		else if (this.spaceship.weaponsEnergy == 4) {
			this.coolingQuantite = 2;
			this.overheatQuantite = 3;
			this.fireRate = 9;
		}
	}
	autoCooling() {
		if (this.nextCooling > 0) {
			this.nextCooling -=1;
		}
		if (this.nextCooling <= 0) {
			this.nextCooling = 0;
			if (this.overheat > 0) {
				this.overheat -= this.coolingQuantite;
			}
			else {
				this.overheat =0;
			}
			this.nextCooling = this.coolingRate;
		}
	}
	unsheatheWeapon() {
		super.unsheatheWeapon();
	}
	sheatheWeapon() {
		super.sheatheWeapon();
	}
}