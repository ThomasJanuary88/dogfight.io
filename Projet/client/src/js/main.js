//	Création du controlleur après avoir chargé
//	la page web.

let optionsSoundOn = false

window.onload = function() {
	controller = new Controller();
	document.getElementById("musicSlider").value = 0.1;
	updateMusicVolume(document.getElementById("musicSlider").value);
	resizeLoginContainer();

}






// A mettre dans view si possible
function updateMusicVolume(volume) {
	document.getElementById("music").volume = volume; 
}

function optionsSound(volume) {
	if (!optionsSoundOn) {
		var audio = new Audio('assets/audio/optionsSoundOff.mp3');
		optionsSoundOn = true;
	}
	else {
		var audio = new Audio('assets/audio/optionsSoundOn.mp3');
		optionsSoundOn = false;
	}
	audio.play();
}

function resizeLoginContainer() {
	let top = document.getElementById("loginContainer").style.top;
	top = top.substring(0, top.length-2);

	if (window.innerHeight/650 <= 1) {
		document.getElementById("loginContainer").style.top = window.innerHeight-650 + "px";
		document.getElementById("loginContainer").style.transform = 'scale(' + window.innerHeight/650*0.9 +')';
		document.getElementById("loginContainer").style['-o-transform'] = 'scale(' + window.innerHeight/650*0.9 +')';
		document.getElementById("loginContainer").style['-webkit-transform'] = 'scale(' + window.innerHeight/650*0.9 +')';
		document.getElementById("loginContainer").style['-moz-transform'] = 'scale(' + window.innerHeight/650*0.9 +')';
	}
}