/*
	Le controller est l'objet permettant de faire le lien
	Le model est l'objet permettant de conserver et de
	mettre à jour toutes les données de la partie
	courante.
*/

class Model {
	constructor(controller) {
		this.controller = controller;

		this.isKeyQPressed = false;
		this.isKeyRPressed = false;
		this.isDigit1Pressed = false;
		this.isDigit2Pressed = false;
		this.isDigit3Pressed = false;
		this.isSpacePressed = false;
		this.entities = {
			"projectile": [],
			"otherPlayer": {},
			"showDamageDealt" : []
		};
		this.emitRate = 2;
		this.nextEmit = 0;
	}

	setBounds() {
		// En haut, a droite, en bas, a gauche
		this.bounds = [0 + this.player.spaceship.height/2,
						2500 - this.player.spaceship.width/2,
						2500 - this.player.spaceship.height/2,
						0 + this.player.spaceship.width/2];
	}

	tick() {

		// Empecher le vaisseau de sortir de la map
		if (this.player.spaceship.x >= this.bounds[1]) {
			if (this.player.spaceship.xVelocity > 0) {
				this.player.spaceship.xVelocity = 0;
			}
		}
		if (this.player.spaceship.x <= this.bounds[3]) {
			if (this.player.spaceship.xVelocity < 0) {
				this.player.spaceship.xVelocity = 0;
			}
		}
		if (this.player.spaceship.y >= this.bounds[2]) {
			if (this.player.spaceship.yVelocity > 0) {
				this.player.spaceship.yVelocity = 0;
			}
		}
		if (this.player.spaceship.y <= this.bounds[0]) {
			if (this.player.spaceship.yVelocity < 0) {
				this.player.spaceship.yVelocity = 0;
			}
		}

		// Créer  des projectile si les armes tirent
		for (let key in this.player.spaceship.weapons) {
			let weapon = this.player.spaceship.weapons[key];
			if (weapon.fired) {
				let projectile = weapon.createProjectile();
				this.emitProjectile(projectile);
				this.createProjectile(projectile);
			}
		}
		// Parcours le dico des entities et appliquent les actions en fonction de la clé
		for (let key in this.entities) {
			switch (key) {
				case "projectile" :
					for (let i = 0; i < this.entities[key].length; i++) {
						let entity = this.entities[key][i];
						entity.tick();
						this.calculDistanceBetween(entity);
						if (!entity.alive) {
				            this.entities[key].splice(i, 1);
				            i--;
						}
					}
				break;

				case "showDamageDealt" :
					for (let i = 0; i < this.entities[key].length; i++) {
						let entity = this.entities[key][i];
						entity.tick();
						if (!entity.alive) {
				            this.entities[key].splice(i, 1);
				            i--;
						}
					}
				break;

				case "otherPlayer" :
					for (let player in this.entities[key]) {
						let entity = this.entities[key][player];
						entity.tick();
					}
				break;
			}
		}
		if (this.player.spaceship.shield.hp <= 0) {
			this.emitShield(this.player.spaceship.shield.activate);
		}
		if (this.player.spaceship.hp <= 0) {
			this.gameOver();
		}
		this.player.spaceship.tick();

		if (this.player.spaceship.shield.hp == 0) {
			this.player.spaceship.shield.hp = 1;
			this.emitShield(this.player.spaceship.shield.activate);
			this.controller.view.playSound("assets/audio/shieldReloading.mp3", 0.5);
		}
		if (this.player.spaceship.weapons[1].reloadNeed) {
			this.controller.view.playSound("assets/audio/machineGunReload.mp3", 0.4);
		}
		if (this.player.spaceship.weapons[0].overheat >= this.player.spaceship.weapons[0].maxOverheat) {
			this.player.spaceship.weapons[0].overheat = this.player.spaceship.weapons[0].maxOverheat-1;
			this.controller.view.playSound("assets/audio/laserCannonOverheat.mp3", 0.4);
		}

		this.updateAngle(this.player.spaceship);
	}

	gameOver() {
		this.controller.gameOver();
	}

	startEmitInformation() {
		setInterval(() => {
			this.controller.emitUpdatePlayerInformation(this.player);
		}, 15);
	}

	updateOtherPlayer(id, otherPlayer) {
		this.entities['otherPlayer'][id].x = otherPlayer.x;
		this.entities['otherPlayer'][id].y = otherPlayer.y;
		this.entities['otherPlayer'][id].angle = otherPlayer.angle;
		this.entities['otherPlayer'][id].score = otherPlayer.score;
		this.entities['otherPlayer'][id].shield.hp = otherPlayer.shieldHp;
	}

	calculDistanceBetween(entity) {
		if (entity.tag == "otherPlayer") {
			let currentDistance = Math.sqrt(Math.pow(entity.x - this.player.spaceship.x, 2)) + Math.sqrt(Math.pow(entity.y - this.player.spaceship.y, 2));
			if ( currentDistance < 60){

				let damage;

				if (this.player.spaceship.shield.activate) {
					if (entity instanceof Bullet) {
						damage = entity.damage*2;
						this.player.spaceship.hit(damage);
					}
					else if (entity instanceof Laser) {
						damage = entity.damage;
						this.player.spaceship.hit(damage);
					}
					this.entities["showDamageDealt"].push(new ShowDamageDealt(this.player.spaceship.x, this.player.spaceship.y, damage, "shieldActivate"));
				}
				else {
					if (entity instanceof Bullet) {
						damage = entity.damage;
						this.player.spaceship.hit(damage);
					}
					else if (entity instanceof Laser) {
						damage = entity.damage*2;
						this.player.spaceship.hit(damage);
					}
					this.entities["showDamageDealt"].push(new ShowDamageDealt(this.player.spaceship.x, this.player.spaceship.y, damage, "shieldUnactivate"));
				}
				entity.alive = false;
			}
		}
		else {
			for (let key in this.entities["otherPlayer"]) {
				let otherPlayer = this.entities["otherPlayer"][key];
				let currentDistance = Math.sqrt(Math.pow(entity.x- otherPlayer.x, 2)) + Math.sqrt(Math.pow(entity.y - otherPlayer.y, 2));
				if ( currentDistance < 60){

					let damage;

					if (otherPlayer.shieldActivate) {
						if (entity instanceof Laser) {
							this.player.score += 1;
							damage = entity.damage;
						}
						else if (entity instanceof Bullet) {
							this.player.score += 2;
							damage = entity.damage*2;
						}
						this.entities["showDamageDealt"].push(new ShowDamageDealt(otherPlayer.x, otherPlayer.y, damage, "shieldActivate"));
					}
					else {
						otherPlayer.hit();
						if (entity instanceof Laser) {
							this.player.score += 2;
							damage = entity.damage*2;
						}
						else if (entity instanceof Bullet) {
							this.player.score += 1;
							damage = entity.damage;
						}
						this.entities["showDamageDealt"].push(new ShowDamageDealt(otherPlayer.x, otherPlayer.y, damage, "shieldUnactivate"));
					}
					entity.alive = false;
					break;
				}
			}	
		}
	}

	handleInput(button) {
		if (button ===  "KeyW"){
			if (this.player.spaceship.y >= this.bounds[0]) {
				this.player.moveUp();
			}
		}
		if (button ===  "KeyD"){
			if (this.player.spaceship.x <= this.bounds[1]) {
				this.player.moveRight();
			}
		}
		if (button ===  "KeyS"){
			if (this.player.spaceship.y <= this.bounds[2]) {
				this.player.moveDown();
			}
		}
		if (button ===  "KeyA"){
			if (this.player.spaceship.x >= this.bounds[3]) {
				this.player.moveLeft();
			}
		}
		if (button ===  "KeyR" && !this.isKeyRPressed){
			if (this.player.spaceship.weapons[1].magazine < this.player.spaceship.weapons[1].defaultMagazine
				&& !this.player.spaceship.weapons[1].isReloading) {
				this.player.spaceship.weapons[1].reloadNeed = true;
				this.player.spaceship.weapons[1].isReloading = true;

				this.controller.view.playSound("assets/audio/machineGunReload.mp3", 0.3);
				this.isDigit1Pressed = true;
			}
		}
		if (button === "Digit1" && !this.isDigit1Pressed) {
			this.player.boostWeapons();
			this.isDigit1Pressed = true;


			this.controller.view.playSound("assets/audio/beepsWeapon.mp3", 0.3);
		}
		if (button === "Digit2" && !this.isDigit2Pressed) {
			this.player.boostShield();
			this.isDigit2Pressed = true;


			this.controller.view.playSound("assets/audio/beepsShield.mp3", 0.3);
		}
		if (button === "Digit3" && !this.isDigit3Pressed) {
			this.player.boostEngine();
			this.isDigit3Pressed = true;


			this.controller.view.playSound("assets/audio/beepsEngine.mp3", 0.3);
		}
		if (button === 'leftClick') {
			this.player.fire();
		}
		if (button === 'Space' && !this.isSpacePressed) {
			this.player.useShield();
			this.isSpacePressed = true;
			this.emitShield(this.player.spaceship.shield.activate);

			if (this.player.spaceship.shield.activate) {
				this.controller.view.playSound("assets/audio/shieldDesactivate.mp3", 0.5);
			}
			else if (!this.player.spaceship.shield.activate){
				this.controller.view.playSound("assets/audio/shieldActivate.mp3", 0.5);
			}
		}
		if (button === 'KeyQ' && !this.isKeyQPressed) {
			this.player.switchWeapon();
			this.isKeyQPressed = true;
			if (this.player.spaceship.weapons[0].unsheathe) {
				this.controller.view.playSound("assets/audio/weaponLaserCannon.mp3", 0.5);
			}
			else if (this.player.spaceship.weapons[1].unsheathe) {
				this.controller.view.playSound("assets/audio/weaponMachineGun.mp3", 0.5);
			}
		}
	}

	keyUp(key) {
		switch (key) {
			case 'Space' :
				this.isSpacePressed = false;
				this.emitShield(this.player.spaceship.shield.activate);
			break;

			case 'KeyQ' :
    			this.isKeyQPressed = false;
			break;

			case 'Digit1' :
    			this.isDigit1Pressed = false;
			break;
			case 'Digit2' :
    			this.isDigit2Pressed = false;
			break;
			case 'Digit3' :
    			this.isDigit3Pressed = false;
			break;
		}
	}

	createOtherShield(data) {
		this.entities['otherPlayer'][data.id].useShield(data.shieldState);
	}

	emitShield(shieldState) {
		this.controller.emitShield(shieldState);
	}

	createProjectile(projectile) {
		this.entities["projectile"].push(projectile);

		if (projectile instanceof Laser) {
			this.controller.view.playSound("assets/audio/laserSound"+Math.floor((Math.random() * 3) + 1)+".mp3", 0.25);
		}
		else if (projectile instanceof Bullet) {
			this.controller.view.playSound("assets/audio/bulletSound"+Math.floor((Math.random() * 4) + 1)+".mp3", 0.25);
		}
	}

	createOtherProjectile(data) {
		let projectile = null;
		switch (data.type) {
			case 'Laser' :
				projectile = new Laser (data.x, data.y, data.angle, data.tag);
			break;

			case 'Bullet' :
				projectile = new Bullet (data.x, data.y, data.angle, data.tag);
			break;
		}
		this.createProjectile(projectile);
	}

	emitProjectile(projectile) {
		this.controller.emitProjectile(projectile, projectile.constructor.name);
	}

	setMousePosition(x, y) {
		this.xMouse = x;
		this.yMouse = y;
	}

	createPlayer(data) {
		this.player = new Player(data.x, data.y, data.name == "" ? "Unknow" : data.name, data.img, data.id, data.nameColor);
	}

	updateAngle(entity) {
		entity.angle = Math.atan2(this.yMouse - entity.y, this.xMouse - entity.x);
	}

	createOtherPlayer(id, data) {
		this.entities['otherPlayer'][id] = new OtherPlayer(data.x, data.y, data.name == "" ? "Unknow" : data.name, data.img, data.score, data.nameColor);
	}
	deleteOtherPlayer(data) {
        delete this.entities['otherPlayer'][data.id];
	}
}