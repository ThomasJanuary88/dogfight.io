class Generator{
	constructor(spaceship) {
		this.maxEnergyPerParts = 4;
		this.spaceship = spaceship;
	}
	boostWeapons() {
		if (this.spaceship.weaponsEnergy < this.maxEnergyPerParts) {
			this.spaceship.weaponsEnergy  += 1;
			if (this.spaceship.shield.energy >= this.spaceship.engine.energy) {
				this.spaceship.shield.energy -= 1;
			}
			else {
				this.spaceship.engine.energy -= 1;
			}
			this.updatePowerParts();
		}
	}
	boostEngine() {
		if (this.spaceship.engine.energy < this.maxEnergyPerParts) {
			this.spaceship.engine.energy += 1;
			if (this.spaceship.weaponsEnergy  >= this.spaceship.shield.energy) {
				this.spaceship.weaponsEnergy  -= 1;
			}
			else {
				this.spaceship.shield.energy -= 1;
			}
			this.updatePowerParts();
		}
	}
	boostShield() {
		if (this.spaceship.shield.energy < this.maxEnergyPerParts) {
			this.spaceship.shield.energy += 1;
			if (this.spaceship.engine.energy >= this.spaceship.weaponsEnergy ) {
				this.spaceship.engine.energy -= 1;
			}
			else {
				this.spaceship.weaponsEnergy  -= 1;
			}
			this.updatePowerParts();
		}
	}
	updatePowerParts() {
		for (let i = 0; i < this.spaceship.weapons.length; i++) {
			this.spaceship.weapons[i].updatePower();
		}
		this.spaceship.engine.updatePower();
		this.spaceship.shield.updatePower();
	}
}