class Player {
	constructor(x, y, name, imgPath, id, nameColor) {
		this.id = id;
		this.name = name;
		this.spaceship = new Spaceship(x, y, imgPath);
		this.score = 0;
		this.nameColor = nameColor;
	}
	moveUp() {
		this.spaceship.moveUp();
	}
	moveDown() {
		this.spaceship.moveDown();
	}
	moveRight() {
		this.spaceship.moveRight();
	}
	moveLeft() {
		this.spaceship.moveLeft();
	}
	boostWeapons() {
		this.spaceship.boostWeapons();
	}
	boostEngine() {
		this.spaceship.boostEngine();
	}
	boostShield() {
		this.spaceship.boostShield();
	}
	switchWeapon() {
		this.spaceship.switchWeapon();
	}
	fire() {
		this.spaceship.fire();
	}
	useShield() {
		this.spaceship.useShield();
	}
}